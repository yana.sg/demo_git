import dash
import dash_html_components as html

app = dash.Dash(__name__)

def new_function():
	"""
	My new function
	"""
	return 2
def second_function():
	"""
	My second function
	"""

	return 1
app.scripts.config.serve_locally = True


app.layout = html.Div([
    html.H1('Hello, World!')
])

application = app.server

if __name__ == '__main__':
    application.run(debug=True, port=8080)