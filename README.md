# demo_git

This is a demo project. It has one file with Python code.

1. Click on `dash_reusable_components.py` . We will see the current code.
2. To see all previous version and changes in `dash_reusable_components.py`  we should click on  `History` button. 
![alt text](<step1.png>) 
3. We can see that the file `dash_reusable_components.py` has three commits (snapshots). Each commit has a short description which is good for documentation. Click on the latest commit.
![alt text](<step2.png>) 

3. We can see the diff between current state and last commit.  We can see that it has 7 new additions and 2 deletions in the source code.
![alt text](<step3.png>) 
